this['/index'] = this['/index'] || {};
this['/index'].js = (function () {
  'use strict';

  class Command {
    constructor({ name = 'Command', execute = function() {}, undo = function() {}, meta = {} }) {
      this.name = name;
      this.execute = execute;
      this.undo = undo;
      this.meta = meta;
    }
  }

  const modules = {
    Command,
  };

  return modules;

}());

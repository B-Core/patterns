(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global['/Command'] = global['/Command'] || {}, global['/Command'].js = factory());
}(this, (function () { 'use strict';

  class Command {
    constructor({ name = 'Command', execute = function() {}, undo = function() {}, meta = {} }) {
      this.name = name;
      this.execute = execute;
      this.undo = undo;
      this.meta = meta;
    }
  }

  return Command;

})));

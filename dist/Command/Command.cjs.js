'use strict';

class Command {
  constructor({ name = 'Command', execute = function() {}, undo = function() {}, meta = {} }) {
    this.name = name;
    this.execute = execute;
    this.undo = undo;
    this.meta = meta;
  }
}

module.exports = Command;

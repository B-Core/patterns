(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global['/index'] = global['/index'] || {}, global['/index'].js = factory());
}(this, (function () { 'use strict';

  class Command {
    constructor({ name = 'Command', execute = function() {}, undo = function() {}, meta = {} }) {
      this.name = name;
      this.execute = execute;
      this.undo = undo;
      this.meta = meta;
    }
  }

  const modules = {
    Command,
  };

  return modules;

})));

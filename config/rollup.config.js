const globby = require('globby');

const baseConfigs = globby.sync('src/**/*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  name: inputFile.substr((inputFile.lastIndexOf('/') || 0)),
  entry: inputFile,
  dest: inputFile.replace('src', 'dist'),
  format: 'es'
}));

const umdConfigs = globby.sync('src/**/*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  name: inputFile.substr((inputFile.lastIndexOf('/') || 0)),
  entry: inputFile,
  dest: inputFile.replace('src', 'dist').replace('.js', '.umd.js'),
  format: 'umd'
}));

const iifeConfigs = globby.sync('src/**/*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  name: inputFile.substr((inputFile.lastIndexOf('/') || 0)),
  entry: inputFile,
  dest: inputFile.replace('src', 'dist').replace('.js', '.iife.js'),
  format: 'iife'
}));

const cjsConfigs = globby.sync('src/**/*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  name: inputFile.substr((inputFile.lastIndexOf('/') || 0)),
  entry: inputFile,
  dest: inputFile.replace('src', 'dist').replace('.js', '.cjs.js'),
  format: 'cjs'
}));

export default [
  ...baseConfigs,
  ...umdConfigs,
  ...iifeConfigs,
  ...cjsConfigs
];
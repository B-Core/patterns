/* eslint func-names: 0 */

import Command from './Command';

describe('Command', () => {
  test('should create a command', () => {
    const subject = {
      test: [],
    };

    const command = new Command({
      execute: jest.fn(function (a) { return this.meta.test.push(a); }),
      undo: jest.fn(function () { return this.meta.test.pop(); }),
      meta: subject,
    });

    expect(command).toBeInstanceOf(Command);

    command.execute('Hello');
    expect(command.execute).toHaveBeenCalledWith('Hello');
    expect(subject).toEqual(command.meta);
    expect(subject.test).toEqual(command.meta.test);

    command.undo();
    expect(command.undo).toHaveBeenCalled();
    expect(subject).toEqual(command.meta);
    expect(subject.test).toEqual(command.meta.test);

    const testCommand = new Command({
      name: 'testCommand',
    });

    expect(testCommand).toBeInstanceOf(Command);
    expect(testCommand.name).toEqual('testCommand');
    expect(testCommand.execute).toBeInstanceOf(Function);
    expect(testCommand.undo).toBeInstanceOf(Function);
    expect(testCommand.meta).toBeInstanceOf(Object);

    expect(() => testCommand.execute()).not.toThrow();
    expect(() => testCommand.undo()).not.toThrow();
  });
});
